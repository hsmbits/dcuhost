
#ifndef ____debug
#define ____debug

#include <stdio.h>
#define debug_enable	1

extern int debug_level;

#if debug_enable
#define debug(...) if(debug_level>1) {printf(__VA_ARGS__);}
#define info(...) if( debug_level>0) {printf(__VA_ARGS__);}
#else
#define debug(...) (void) (__VA_ARGS__)
#define info(...)  (void) (__VA_ARGS__)
#endif

#endif

