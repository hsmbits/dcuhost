

#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/socket.h>
#include <pthread.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

#include "goptions.h"
#include "debug.h"


pthread_t KeyServerThread;
void* KeyServerThreadFunction(void *Ctx);
pthread_mutex_t sMutex;

#define SOCK_BUFLEN 2048
#define DEFAULT_REMOTE_PORT	"4433"

static int Stop = 0;
char SendBuffer[SOCK_BUFLEN];
char ReceiveBuffer[SOCK_BUFLEN];
int BytesAssembled = 0;
void TunnelEapolData(uint8_t BrIndex, uint8_t *Node, uint8_t *Buf, uint16_t len);

int SendLength;



typedef struct CLSOCK_TYPE {
	int s;
	uint8_t  Node[8];
	CLSOCK_TYPE *Next,*Prev;
	uint16_t Port;
	short AssociatedBr;
	uint8_t Shutdown;
}CLSOCK, *PCLSOCK;

PCLSOCK SockList;
PCLSOCK AssociatedSocketArray[FD_SETSIZE];

int StopKeyServer() {
	printf("net error: %d\n", errno);
	if(KeyServerThread)
		pthread_cancel(KeyServerThread);
	return 0;
}

int StartKeyServer(void)
{
    pthread_mutex_init(&sMutex, NULL);
	if(pthread_create(&KeyServerThread, NULL, KeyServerThreadFunction, NULL)!=0) {
		printf("Error starting border router thread:%d\n",errno);
		exit(0);
	}

	return 0;
}


void DeleteClientSocket(PCLSOCK S) {

	if (S == SockList) {
		SockList = NULL;
	}
	else if((S->Next == NULL) && (S->Prev != NULL)) {
		S->Prev->Next = NULL;
	}
	else if ((S->Next != NULL) && (S->Prev == NULL)) {
		SockList = S->Next;
	}
	else {
		S->Prev->Next = S->Next;
		S->Next->Prev = S->Prev;
	}
	shutdown(S -> s, SHUT_RDWR);
	close(S->s);
	delete S;
}

PCLSOCK _StartNewTlsSession(uint8_t *Node, short AssociatedBr) {
	int res;
	PCLSOCK ps;

	for (ps = SockList; ps != NULL; ps = ps->Next) {
		if (ps->AssociatedBr == AssociatedBr) {
			if (memcmp(ps->Node, Node, 8) == 0) {
				DeleteClientSocket(ps);
				break;
			}
		}
	}

	ps = new CLSOCK;
	if (!ps) {
		printf("Cannot allocate CLSOCK\n");
		return NULL;
	}

	// Create a SOCKET for connecting to server
	ps->s = socket(AF_INET, SOCK_STREAM, 0);
	if (ps->s < 0) {
		printf("socket failed with error: %d\n", errno);
		goto _err;
	}

	// Connect to server.
	struct sockaddr_in srv;
	memset(&srv, 0, sizeof(sockaddr_in));
	srv.sin_family = AF_INET;
	srv.sin_port = htons(tlsport);
	if(inet_pton(AF_INET, tlsserveraddress, &srv.sin_addr)<=0)
    {
        printf("\n inet_pton error occured\n");
        goto _err;
    }

	res = connect(ps->s, (const sockaddr*) &srv, sizeof(srv));
	if (res < 0 ) {
		close(ps->s);
		info("socket closed:%d\n", errno);
		goto _err;
	}
	{
		struct sockaddr_in sin;
		socklen_t len = sizeof(sin);
		if (getsockname(ps->s, (struct sockaddr *)&sin, &len) == -1) {
			perror("getsockname");
			goto _err;
		}
		else {

			// Add to list now
			if (SockList == NULL) {
				SockList = ps;
				ps->Next = NULL;
				ps->Prev = NULL;
			}
			else {
				ps->Next = SockList;
				ps->Prev = NULL;
				SockList->Prev = ps;
				SockList=ps;
			}
			memcpy(ps->Node, Node, 8);
			ps->Port = ntohs(sin.sin_port);
			ps->AssociatedBr = AssociatedBr;
			printf("New connection on port %d\n", ps->Port);
			return ps;
		}
	}

_err:
	delete ps;
	return NULL;
}

void _DisconnectTlsSession(uint8_t *Node, short AssociatedBr) {
	int res;
	PCLSOCK ps;

	for (ps = SockList; ps != NULL; ps = ps->Next) {
		if (ps->AssociatedBr == AssociatedBr) {
			if (memcmp(ps->Node, Node, 8) == 0) {
				DeleteClientSocket(ps);
				break;
			}
		}
	}
}

void* StartNewTlsSession(uint8_t *Node, short Inst) {
	unsigned long long longid;
	PCLSOCK ps;
	for (int n = 0; n < 8; n++) ((unsigned char*)&longid)[7 - n] = Node[n];
	printf("\n<Starting a new TLS session br=%d, Node=%llx>\n", Inst, longid);
	//if (longid == 0x101) return NULL;
	pthread_mutex_lock(&sMutex);
	ps = _StartNewTlsSession(Node, Inst);
	pthread_mutex_unlock(&sMutex);
	return ps;
}

void DisconnectTlsSession(void *ctx, uint8_t *Node, short AssociatedBr) {

	unsigned long long longid;
	for (int n = 0; n < 8; n++) ((unsigned char*)&longid)[7 - n] = Node[n];
	printf("\n<Terminating TLS session br=%d, Node=%llx>\n", AssociatedBr, longid);

	pthread_mutex_lock(&sMutex);
	_DisconnectTlsSession(Node, AssociatedBr);
	pthread_mutex_unlock(&sMutex);

}


void* KeyServerThreadFunction(void *Ctx) {

void TunnelEapolData(uint8_t br, uint8_t *Node, uint8_t *buf, uint16_t nb);

	int iResult,r;

    while (!quit) {
		fd_set fd;
		int maxs = 0;
		int fdcnt = 0;

		static timeval instant = { 0,10*1000 };

		pthread_mutex_lock(&sMutex);

		FD_ZERO(&fd);
		for (PCLSOCK s = SockList; s != NULL; s = s->Next) {
			FD_SET(s->s,&fd);
			AssociatedSocketArray[fdcnt++] = s;
			if(s->s > maxs) maxs = s->s;
		}

		pthread_mutex_unlock(&sMutex);


		if (fdcnt == 0) {
			usleep(10000);
			continue;
		}

		r = select(maxs+1, &fd, 0, 0, &instant);
		if (r == -1)
		{
			printf("Sock error: %d\n", errno);
			continue;
		}
		if (r == 0) continue;
		//printf("%d of %d\n", fd.fd_count, nfd);

		for (uint8_t n = 0; n < fdcnt; n++) {
			// Find Sock structure that has data
			PCLSOCK ps;
			pthread_mutex_lock(&sMutex);
			for (ps = SockList; ps; ps = ps->Next) {
                if(FD_ISSET(ps->s,&fd))
                    break;
			}
			pthread_mutex_unlock(&sMutex);
			if (ps == NULL) continue;

			memset(ReceiveBuffer, 0, SOCK_BUFLEN);
			int nb = recv(ps->s, ReceiveBuffer, SOCK_BUFLEN, 0);
			if (nb == -1) {
				printf("Error receiving (sock) data %d\n", errno);
				//DeleteClientSocket(ps);
			}
			else if (nb>0) {
				long long longid;
				for (int n = 0; n < 8; n++) ((unsigned char*)&longid)[7 - n] = ps->Node[n];
				TunnelEapolData((uint8_t)ps->AssociatedBr, ps->Node, (uint8_t*)ReceiveBuffer, (uint16_t)nb);
				printf("\n<tls-send-node %d bytes for %llx>\n", nb, longid);
				if (ReceiveBuffer[0] == 0) printf("keys?\n");
			}
			if (nb == 0) {
				pthread_mutex_lock(&sMutex);
				DeleteClientSocket(ps);
                pthread_mutex_unlock(&sMutex);
			}
		}

	}

SockError:
	StopKeyServer();
	exit(0);
}

void ReverseCopyFunction(void *dst, void *src, int n) {
	unsigned char *s = (unsigned char*)src + n - 1;
	unsigned char *d = (unsigned char*)dst;
	while (n>0) {
		*d++ = *s--;
		n--;
	}
}

void ShowActiveSessions() {

	PCLSOCK ps;
	int n = 0;

	pthread_mutex_lock(&sMutex);

	// Initial 8 bytes indicate the context Node address
	for (ps = SockList; ps != NULL; ps = ps->Next) {
		unsigned long long ll;
		ReverseCopyFunction(&ll, ps->Node, 8);
		printf("Session[%d] - %llx\n", n++, ll);
	}

	printf("Active Sessions: %d\n", n);
	pthread_mutex_unlock(&sMutex);

}
 int SendDataToServer(unsigned char *buf, int len) {
	PCLSOCK ps;

	if (len > SOCK_BUFLEN) {
		return 1;
	}


	pthread_mutex_lock(&sMutex);

	// Initial 8 bytes indicate the context Node address
	for (ps = SockList; ps != NULL; ps = ps->Next) {
		if (memcmp(ps->Node, buf, 8) == 0) {
			break;
		}
	}
	pthread_mutex_unlock(&sMutex);
	if (ps != NULL) {
		len -= 8;
		if (len) {
            if (send(ps->s, (const char*)buf + 8, len, 0) == -1) {
				printf("Send Failed\n");
			}
			else {
				long long longid;
				for (int n = 0; n < 8; n++) ((unsigned char*)&longid)[7 - n] = ps->Node[n];
				//Sleep(500);
				printf("\n<tls-send-net %d bytes for %llx>\n", len, longid);
			}
		}
	}
	else {
		printf("<No active session, TLS start?>\n");
	}

	return 0;
}


#define TBUF_LEN	2048
int SendDataToServer(unsigned char *buf, int len);
void* StartNewTlsSession(uint8_t *Node, short Inst);
void DisconnectTlsSession(void *ctx, uint8_t *Node, short AssociatedBr);
typedef struct CONSOLE_DATA_PKT_TYPE {
	unsigned char BinHdr;
	unsigned char Id;
	short Length;
	unsigned char Tag;			// Additional information
	unsigned char Data;
}CONSOLE_DATA_PKT, *PCONSOLE_DATA_PKT;
float ct;
static int tbufi = 0;
static int assemble;
static float tm = 0;
static short tlen = 0;
short tbuf[TBUF_LEN/2];
PCONSOLE_DATA_PKT conp = (PCONSOLE_DATA_PKT)tbuf;
unsigned char *tp = (unsigned char*)tbuf;
short ti;


int TlsProcessor(char c)
{
float GetTickCount();

	int consumed = 0;

	tp[ti++] = c;
	ct = GetTickCount();

	switch (assemble) {
	case 0:
		if (ti == 2) {
			if (conp->BinHdr == '!' && conp->Id == 5) {
				assemble = 1;
				//debug("\nTLS-BEGIN\n");
			}
			else {
				ti = 1;
				tp[0] = c;
				//debug("\nTLS-RESET\n");
			}
		}
		break;

	case 1:
		consumed = 1;
		if (ti == 4) {
			if (conp->Length > 0) {
				assemble = 2;
			}
		}
		goto check_timeout;

	case 2:
		consumed = 1;
		if (ti == (4 + conp->Length)) {
			if (conp->Tag == 0) {
				SendDataToServer(&conp->Data, conp->Length - 1);
			}
			else if (conp->Tag == 1) {
				StartNewTlsSession(&conp->Data, 0);
			}
			else if (conp->Tag == 2) {
				DisconnectTlsSession(NULL, &conp->Data, 0);
			}
			else {
				printf("\n<Unknown tls tag>\n");
				for (int i = 0; i < 4 + conp->Length; i++) {
					printf("%02x ", tp[i]);
				}
				printf("\n");
			}
			ti = 0;
			assemble = 0;
			break;
		}
		else {
			check_timeout:
			if ((ct - tm) > .5) {
				info("\ntls-assemble-abort(%d bytes)\n",conp->Length);
				ti = 0;
				assemble = 0;
			}
		}
	}

	tm = ct;
	return consumed;

}
