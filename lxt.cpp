
/*
	lxt.cpp created by Mukunda HS 12/11/2018
*/

#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>
#include <sys/time.h>

#include "goptions.h"
#include "option.h"
#include "debug.h"

void initterminal(void);
void resetterminal(void);
int kbhitw(void);
void* keyboardthread(void *a);
void* serialthread(void *a);

void log(char ch);
void openlogfile();
void transferfile(char *fname);
void ResetAckNack();
int GetAckNack();
void processacknack(char ch);
int StartKeyServer();
void LoadMacros();
void sendser(char *p, int n);
void togglehexmode();
void firmwareupdateprocess();

int TlsEnable;

/* Configuration data */
struct Config_ {
	char portname[32];
	int baudrate;
	int fd;
	char rbuf[2048];
    pthread_t serthread;
    pthread_t keyboardthread;
    pthread_mutex_t sermutex, keymutex;

} C;
int tab = 0;

#define MAX_MACRO_LEN 256
#define MAX_MACROS 26
int serialbaudrate, serialnumbits, serialnumstopbits, serialparity;
void (*sercallback)(char c);

struct MACROBUF_ {
	char Buf[MAX_MACRO_LEN];
	short Length;
} Macros[MAX_MACROS];


int main(int N, char *A[]) {

	int n = 1;

	if (N > 1) {
		while (n < N) {
			//printf("A[%d]=%s\n", n, A[n]);
			if(A[n][0] != '-') {
                printf("options should start with -\n");
                return -1;
			}
			if (A[n][1] == 'p') {
				strncpy(C.portname, A[n+1], 32);
				n+=2;
			}

			else {
				printf("%c is not an option\n", A[n][0]);
				n++;
			}
		}
	}
	else {
		printf("lxt -p /dev/term_name\n");
		return -1;
	}

	setoptionsfile((char*)"lxterm.cfg");
	getoption("debug-level", "%d", &debug_level);
    getoption("tls-server", "%s", tlsserveraddress);
    getoption("macros-file", "%s", macrosfilename);
    getoption("tls-port", "%d", &tlsport);
    getoption("serial-params"," %d %d %d %d", &serialbaudrate, &serialnumbits, &serialnumstopbits, &serialparity);
    LoadMacros();

	openlogfile();
    sercallback = NULL;
	pthread_mutex_init(&C.keymutex,NULL);
	pthread_mutex_init(&C.sermutex,NULL);
	pthread_create(&C.serthread,NULL,serialthread,NULL);
	pthread_create(&C.keyboardthread,NULL,keyboardthread,NULL);
    StartKeyServer();

    while(!quit) {
        sleep(1);
    }

    if(C.keyboardthread) pthread_cancel(C.keyboardthread);
    if(C.serthread) pthread_cancel(C.serthread);

	printf("Hit enter to continue\n");
	resetterminal();
	//getchar();
	fcloseall();
	return 0;
}

struct termios tie;
void echoenable() {
    tcgetattr(STDIN_FILENO, &tie);
    tie.c_lflag |= ECHO;
    tcsetattr(STDIN_FILENO,TCSANOW, &tie);
}

void echodisable() {
    tie.c_lflag = 0;
    tcsetattr(STDIN_FILENO,TCSANOW,&tie);
}
void processctrl(char ch) {

    do {
        if(ch == 'x') {
            printf("\nDo you want to quit(y/n):");
            if(getchar()=='y') quit = 1;

        }
        else if(ch=='s') {
            char filename[256];
            echoenable();
            printf("\nSend file name:");
            int ni = scanf("%s",filename);
            printf("scanf ret:%d\n",ni);
            if( ni == 1){
                transferfile(filename);
            }
            else {
                printf("Bad file name %s\n", filename);
            }
            echodisable();
        }
        else if(ch=='m') {
            ch = getchar();
            if((ch>='a') && (ch<='z')) {
                int m = ch-'a';

                if(Macros[m].Length) {
                    sendser(Macros[m].Buf,Macros[m].Length);
                }
                else {
                    printf("No macro registered for [m]-[%c]\n",ch);
                }
                break;
            }
            else if(ch=='1') {
                printf("Macros will e refreshed\n");
                LoadMacros();
            }
            else if(ch=='2') {
                printf("\nMacros loaded\n");
                for (int n = 0; n < MAX_MACROS; n++) {
                    if (Macros[n].Length) {
                        printf("%c - %s\n",'a'+n, Macros[n].Buf);
                    }
                }
            }
            else printf("No function defined for  [m]-[%c]\n",ch);
        }
        else if(ch=='d') {
            togglehexmode();
        }
        else if(ch=='U') {
            void sendwisunconfig();
            echoenable();
            sendwisunconfig();
            echodisable();
        }
        else if(ch=='#') {
            echoenable();
            printf("\nDo you really want update alternate firmware (y/n) ");
            if (getchar() == 'y') {
                printf("\n");
                firmwareupdateprocess();
            }
            echodisable();

        }
        else if(ch=='h') {
            extern char *helpbuffer;
            puts(helpbuffer);
        }
        else if(ch=='*') {
            void fileuploadprocess(char *s);
            char fname[256];
            echoenable();
            printf("Send file: ");
            while(1) {
                if(scanf("%s",fname)==0) {
                    printf("Bad filename\n");
                }
                else {
                    fileuploadprocess(fname);
                }
                echodisable();
                break;
            }
        }
    } while(0);

    tab = 0;
    printf("\n");
}

void sendser(char *p, int n) {
    int w = write(C.fd, p, n);
    if(w==-1) {
        perror("serwrite");
    }
}

void* keyboardthread(void *a) {
    initterminal();
    printf("Terminal is now configured\n\n");
    while (1) {
        char ch;

        ch = getchar();

        //printf(" < key:%d>\n", (int) ch);
        if(ch==9) {
            if(tab) {
                sendser(&ch,1);
                tab = 0;
            }
            else {
                printf("key: ");
                tab = 1;
            }
        }
        else {
            if(tab) {
                putchar(ch); putchar('\n');
                processctrl(ch);
            }
            else sendser(&ch,1);
        }

        if(quit) break;
	}
	resetterminal();
	C.keyboardthread = 0;
    printf("end keybpardthread\n");
    fflush(stdout);
	return NULL;
}

/* Terminal input related functions */
static struct termios oldt, newt;
int termset = 0;
/* Initialize new terminal i/o settings */
void initterminal()
{
	tcgetattr(STDIN_FILENO, &oldt);
	newt = oldt;
	newt.c_lflag = 0;//~(ECHO); // | ICANON | ISIG);
	tcsetattr(STDIN_FILENO, TCSANOW, &newt);
	termset = 1;
    atexit(resetterminal);
}

/* Restore old terminal i/o settings */
void resetterminal(void)
{
    if(termset) {
        tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
        termset = 0;
	}
}

unsigned int  brates[2] = {B9600, B115200};
int programport() {

    struct termios t;
    tcgetattr(C.fd,&t);
    if( (serialbaudrate < 0) || (serialbaudrate>1) ) {
        printf("Please check/correct the baud rate setting in lxterm.cfg");
        quit = 1;
        return 1;
    }
    t.c_cflag = brates[serialbaudrate] | CS8 | CLOCAL | CREAD;
    t.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);
    t.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
    t.c_oflag = 0;
    t.c_cc[VTIME] = 0;
    t.c_cc[VMIN] = 1;
    tcflush(C.fd,TCIFLUSH);
    tcsetattr(C.fd,TCSANOW,&t);
    return 0;
}


void* serialthread(void* a) {
int TlsProcessor(char c);

    int q = 1;

    do {
        C.fd = open(C.portname, O_RDWR | O_NOCTTY);
        if (C.fd <= 0) {
            printf("port error\n");
            break;
        }
        printf("port %s has been successfully opened\n", C.portname);
        programport();
        q = 0;
    } while(0);

    if(!q) {
        while(!quit) {
            int n;
            int consumed = 0;
            n = read(C.fd, C.rbuf, 256);
            if(n==-1) {
                perror("serread");
            }
            /*
            if(n==0) {
                usleep(200000);
            }
            */
			for (int i = 0; i < n; i++) {
                char ch = C.rbuf[i];
                processacknack(ch);
                consumed |= TlsProcessor(ch);
                if(sercallback) sercallback(ch);
				//if(!consumed)
                    log(ch);
			}
			fflush(stdout);
			consumed = 0;
        }
    }
    quit = 1;
    C.serthread = 0;
    printf("end serialthread\n");
    fflush(stdout);
    return NULL;
}

double GetTickCount() {
    struct timeval tm;
    double tim;
    gettimeofday(&tm,NULL);
    tim = (double) tm.tv_sec + tm.tv_usec/1000000.0;
    return tim;
}


/* ========================== LOG Related ==========================*/

struct timeval lt;
int WriteTime;
double starttime;

FILE *lf;
char LogFileName[32];

void openlogfile() {
	char buf[128];

	srand(time(NULL));
	system("mkdir -p logs");
	sprintf(LogFileName, "logs/log-%d.log", rand());
	lf = fopen(LogFileName, "wt");
	if (!lf) {
		printf("Unable to open logfile %s for writing\n", buf);
	}
	else {
		printf("Opened logfile %s\n", LogFileName);
	}
	gettimeofday(&lt,NULL);
    starttime = lt.tv_sec + lt.tv_usec/1000000.0;
}

extern "C" void Log(const char *format, ...)
{
	static char buf[2048];
	va_list ArgPtr;

	va_start(ArgPtr, format);

	vsnprintf(buf, 2048, format, ArgPtr);
	va_end(ArgPtr);

	printf("%s",buf);
	fprintf(lf,"%s", buf);
}


unsigned char hexbuf[16];
unsigned char hindex;
int hexmode = 0;
const char *cabuf = "0123456789ABCDEF";

void resethexbuf() {
    memset(hexbuf, 0, 16);
    hindex = 0;
}

void togglehexmode() {
    hexmode = !hexmode;
    if (hexmode) {
        resethexbuf();
        printf("\nEntering hex display mode");
    }
    else printf("\nLeaving hex display mode");
    printf("\n");
}

void printhexmodeoutput(char ch);


void log(char ch) {
    double tm,ltm;

	if (!lf) {
		putchar(ch);
		return;
	}


    gettimeofday(&lt,NULL);
    tm = lt.tv_sec + lt.tv_usec/1000000.0;
    tm = tm - starttime;

	if (!WriteTime) {
		if ((ch == '\n') || (ch == '\r')) {
            if (hexmode) printhexmodeoutput(ch);
            else fputc(ch,stdout);
			fputc(ch, lf);
			WriteTime = 1;
			fflush(lf);
			fflush(stdout);
			ltm = tm;
			return;
		}
	}

	if (WriteTime) {
		fprintf(lf, "%7.3lf ", tm);
	}
    if (hexmode) {
        printhexmodeoutput(ch);
    }
    else {
        fputc(ch, stdout);
    }
	fputc(ch, lf);
	fflush(stdout);
    fflush(lf);
	WriteTime = 0;
    ltm = tm;
}


void printhexmodeoutput(char ch)
{
    int n;
    fputc('\r',stdout);
    hexbuf[hindex++] = ch;
    for (n = 0; n < hindex; n++) {
        unsigned char chu = hexbuf[n];
        fputc(cabuf[chu >> 4], stdout);
        fputc(cabuf[chu & 15], stdout);
        fputc(' ', stdout);
    }
    for (; n < 16; n++) {
        fputc(' ', stdout); fputc(' ', stdout); fputc(' ', stdout);
    }
    printf("    ");
    for (n = 0; n < hindex; n++) {
        char ch = hexbuf[n];
        if (ch >= 32 && ch <= 127) {
            fputc(ch,stdout);
        }
        else fputc('.', stdout);
    }
    if (hindex == 16) {
        resethexbuf();
        printf("\n");
    }
}

/* ========================== Commands Related ==========================*/


void transferfile(char *fname) {

	do {
		FILE *f = fopen(fname, "rb");
		if (!f) {
			printf("File not available\n");
			break;
		}
		else {
            struct stat s;
			stat(fname,&s);
			long size = s.st_size;
			char *b = new char[size];
			if (!b) {
				printf("Memory error\n");
				break;
			}
			else {
				long n = fread(b, 1, size, f);
				if (n != size) {
					delete b;
					printf("File read error\n");
					break;
				}
				else {
					sendser(b, size);
					printf("\nSent %d bytes\n", (int)size);
					fclose(f);
					GetAckNack();
					delete b;
				}
			}
		}
	} while (0);
}

int anflag = 0;
static unsigned long long AckStream = 0x2102010001;
static unsigned long long NackStream = 0x21020300020000;
static unsigned long long stream;
short ecode;
int Ack, Nack;

void ResetAckNack() {
	Ack = Nack = 0;
	stream = 0;
	anflag = 1;
}

int GetAckNack() {
	ecode = 0;
	ResetAckNack();
	for (int n = 0; n < 5; n++) {
		usleep(500000);
		if (Ack) {
			printf("OK\n");
			break;
		}
		else if (Nack) {
			printf("\nError: %d\n", ecode);
			break;
		}
		if (n == 4) {
			printf("No response\n");
		}
	}
	return ecode;
}



void processacknack(char ch) {
    // Receive ACK/NACK
    unsigned long long x;
    stream = (stream << 8) | (unsigned char) ch;
    x = stream & 0x000000ffffffffff;
    if (x == AckStream) {
        Ack = 1;
        anflag = 0;
    }
    else {
        x = stream & 0x00ffffffffff0000;
        if (x == NackStream) {
            Nack = 1;
            ecode = (short) (stream & 0xffff);
            anflag = 0;
        }
    }
    //printf("%llx\n", stream);
}

#include <stdint.h>

void TunnelEapolData(uint8_t br, uint8_t *Node, uint8_t *buf, uint16_t nb) {
	char *B;
	short nbp = nb+9;
	B = new char[nb + 4 + 1 + 8];

	if (!B) {
		printf("Memory allocation error while sending tls data to node\n");
		return;
	}
	B[0] = '!'; B[1] = 5;
	B[2] = ((char*)&nbp)[0];
	B[3] = ((char*)&nbp)[1];
	B[4] = 0;
	memcpy(&B[5], Node, 8);
	memcpy(&B[13], buf, nb);
	nbp += 4;
	sendser(B, nbp );
	delete B;
}


void LoadMacros()
{

    int CharMode=0;
    if(strlen(macrosfilename)==0) {
        printf("macro input file not set\n");
        return;
    }

	FILE *MacFile = fopen(macrosfilename, "rt");
	if (!MacFile) {
		printf("Macro file %s not found\n", macrosfilename);
		return;
	}

	for (int n = 0; n < MAX_MACROS; n++) {
		Macros[n].Length = 0;
	}

	for (int n = 0; n < MAX_MACROS; n++) {
		int num = fgetc(MacFile) - 97;
		fgetc(MacFile);
		if (feof(MacFile)) break;
		CharMode = 0;

		if ((num >= 0) && (num < MAX_MACROS)) {	// Less than 'x'
			int i,in;
			in = 0;
			for (i = 0; i < MAX_MACRO_LEN - 1; i++) {
				if (CharMode == 0) {
					char ch = fgetc(MacFile);
					if ((ch == '\n') || (ch == '\r')) break;
					if (ch == '\\') {
						CharMode = 1;
						continue;
					}
					Macros[num].Buf[in++] = ch;
				}
				else {
					char buf[256];
					unsigned short x;
					if (fscanf(MacFile, "%s", buf) != 1) {
						printf("Error in macro %d\n", num + 1);
						break;
					}
					if (buf[0] == '\\') {
						int sl = strlen(buf)-1;
						CharMode = 0;
						strncpy(&Macros[num].Buf[in], &buf[1], sl);
						in += sl;

						continue;
					}
					if (sscanf(buf, "%hx", &x) != 1) {
						printf("Error in macro %d\n", num + 1);
						break;
					}
					Macros[num].Buf[in++] = x;
				}
			}
			if (in > 1) {
				Macros[num].Length = in;
			}
		}
		else {
			printf("Macros parsed upto line %d\n", n);
			break;
		}
	}
	printf("\nMacros have been refreshed\n");
	fclose(MacFile);
}


void sendwisunconfig()
{
    char cmd[256];
    char fname1[256];
    printf("Enter file name: ");
    if (scanf("%s", fname1) == 1) {
        FILE *f = fopen(fname1, "rb");
        if (!f) {
            printf("File not available\n");
            return;
        }
        snprintf(cmd, 256, "wsnconfig %s 1 wsnconfigtemp", fname1);
        int res = system(cmd);
        if (res == -1) {
            printf("There was an error executing wsncnfig\n");
            return;
        }
        transferfile((char*)"wsnconfigtemp");
        system("rm wsnconfigtemp");
    }
}


int SendBlock(unsigned short BlockNumber);
char dbuf[256];
uint8_t dbufi, dbufr;

void serialcallback(char c) {
    dbuf[dbufi++] = c;
}

int savail() {
    return (dbufi-dbufr);
}

char getdbuf() {
    char ch = dbuf[dbufr++];
    return ch;
}

void resetdbuf() {
    dbufi = dbufr = 0;
}


int _kbhit()
{
	int n;
	ioctl(STDIN_FILENO, FIONREAD, &n);
	return n;
}

void sendserialmessage(char *s) 
{
    int slen = strlen(s);
    if(slen) {
        sendser(s,slen);
    }
    usleep(500000);
    /*
    double tm = GetTickCount();
    while(1) {
        if( (GetTickCount() - tm) > 0.5) return;
        usleep(10000);
    }
    */
}

char *InBuf=NULL;
int bno = 0;

void firmwareupdateprocess()
{
	int state = 1, Length;
	int i, n,rc;
	char c;
	unsigned short BlkNo;
	unsigned char mbuf[512];
	int retry = 0;
    FILE *FwFile = NULL;
    int FwNo, FlashSize, TotalBlocks, TotalSize, NBlocks;
    int Boot;
    int TlsE;
    struct stat s;
    long size;

    resetdbuf();
	Boot = 0;
	printf("Boot ? (y/n): ");
	if (getchar() == 'y') {
		Boot = 1;
	}
	printf("\n");

	FwFile = fopen("fw.bin", "rb");
	if (!FwFile) {
		printf("Please make sure fw.bin is available in working directory\n");
		return;
	}
    sercallback = serialcallback;
    resetdbuf();

    stat("fw.bin",&s);
    size = s.st_size;
    if(size>0)
        TotalSize = size;
    else  {
        printf("File error\n");
        goto endthis;
    }
	printf("File size: %d\n", TotalSize);
    
    InBuf = new char[TotalSize];
    if(!InBuf) {
        printf("Unable to allocate file buffer\n");
       goto endthis;
    }
	rc = fread(InBuf, 1, TotalSize, FwFile);
	if ((rc != TotalSize) || feof(FwFile)) {
		fclose(FwFile);
		printf("File error\n");
	    goto endthis;
	}

	NBlocks = TotalSize / 256 + 1;
	printf("Effective flash size/blocks: %d/%d\n", TotalSize, NBlocks);
    sprintf((char*)mbuf, "\nflash %d %d\n", NBlocks, Boot);
	sendserialmessage((char*)mbuf);

	n = 0;
	rc = 0;
    TlsE = TlsEnable;
	TlsEnable = 0;

    printf("Waiting for client response\n");
    for (int j = 0; j < 500; j++) {		// Wait for 20 Seconds for flash erase operation
        
        if (_kbhit()) {
			if (getchar() == 27) {
				printf("Aborting the flash update process\n");
                goto endthis;
			}
		}

        if(savail()) {
            c = getdbuf();
            if(c==1) goto startblocks;
        }
        else {
            usleep(100000l);
            continue;
        }
		
		
	}
	printf("No response for the request\n");
    goto endthis;

startblocks:
	printf("  Peer acknowledged the request\n");
    bno = 0;
    double to;
	do {
        char ch;
		SendBlock(n);
        to = GetTickCount();
        while(!savail()) {
            if(GetTickCount()-to  > 1) {
                if(rc++ < 6) continue;
                printf("No response\n");
                goto endthis;
            }
            usleep(1000);
        }
        ch = getdbuf();
        if(ch == 'Y') {
            n++;
            rc = 0;
            continue;
        }
        else if(ch == 'N') {
            if(rc++ < 6) continue;
            printf("No response\n");
            goto endthis;
        }
	} while (n < NBlocks);


    usleep(1000000);
	if (n != NBlocks) {
		printf("\nUpdate failed\n");
	}
	else {
		printf("\nUpdate succeeded\n");
	}


endthis:
	TlsEnable = TlsE;
    if(FwFile) fclose(FwFile);
	if(InBuf) delete InBuf;
    InBuf = NULL;
    sercallback = NULL;
    
}



int SendBlock(unsigned short BlockNumber)
{
    char TxBuffer[260];
	int ReadCount = 0;
	int i;
	unsigned short sum1 = 0xaa55;

	TxBuffer[0] = BlockNumber & 0xFF;
	TxBuffer[1] = (BlockNumber >> 8) & 0xFF;
	memcpy(&TxBuffer[2], &InBuf[256 * BlockNumber], 256);

	for (i = 0; i < 258; i++) {
		sum1 += (unsigned char)TxBuffer[i];
	}
	TxBuffer[258] = sum1;
	TxBuffer[259] = sum1 >> 8;


	sendser(TxBuffer, 260);
    printf("\r(%d)Sending block  %04d", bno++,BlockNumber);
	return 0;
}

void fileuploadprocess(char *fname)
{
    int NBlocks, TotalSize;
    unsigned char mbuf[300];
    int rc, TlsE;
    char c;


    FILE *f = fopen(fname,"rb");
    if(!f) {
        printf("Cannot open file\n");
        return;
    }
    struct stat s;
    stat(fname,&s);
    long size = s.st_size;
    char *b = new char[size];
    if (!b) {
        printf("Memory error\n");
        return;
    }
    else {
        long n = fread(b, 1, (size_t)size, f);
        if (n != size) {
            delete b;
            printf("File read error\n");
            fclose(f);
            return;
        }

        sercallback = serialcallback;
        resetdbuf();

        NBlocks = size / 256 + 1;
        printf("Effective flash size/blocks: %d/%d\n", size, NBlocks);
        sprintf((char*)mbuf, "\ncupd %d 1\n", size );
        sendserialmessage((char*)mbuf);

        n = 0;
        rc = 0;
        TlsE = TlsEnable;
        TlsEnable = 0;

        printf("Waiting for client response\n");
        for (int j = 0; j < 200; j++) {		// Wait for 20 Seconds for flash erase operation

            if (_kbhit()) {
                if (getchar() == 27) {
                    printf("Aborting the flash update process\n");
                    goto endthis;
                }
            }

            if(savail()) {
                c = getdbuf();
                if(c==1) goto startblocks;
            }
            else {
                usleep(100000l);
                continue;
            }


        }
        printf("No response for the request\n");
        goto endthis;

    startblocks:
        printf("  Peer acknowledged the request\n");
        bno = 0;
        double to;
        InBuf = b;
        do {
            char ch;
            SendBlock(n);
            to = GetTickCount();
            while(!savail()) {
                if(GetTickCount()-to  > 1) {
                    if(rc++ < 6) continue;
                    printf("No response\n");
                    goto endthis;
                }
                usleep(1000);
            }
            ch = getdbuf();
            if(ch == 'Y') {
                n++;
                rc = 0;
                continue;
            }
            else if(ch == 'N') {
                if(rc++ < 6) continue;
                printf("No response\n");
                goto endthis;
            }
        } while (n < NBlocks);


        usleep(1000000);
        if (n != NBlocks) {
            printf("\nUpdate failed\n");
        }
        else {
            printf("\nUpdate succeeded\n");
        }


    endthis:
        TlsEnable = TlsE;
        if(f) fclose(f);
        if(b) delete b;
        sercallback = NULL;
    }
}

char tlsserveraddress[128];
char macrosfilename[128];
char netname[33];
unsigned int tlsport;
int quit;
int debug_level;

const char *helpbuffer = 
"press [tab] key to enable function keys\n"
"'x'     : Exit this app\n"
"'s'     : Send a file over serial\n"
"'m'     : Send macro sequence registered in macros file\n"
"        : m + (a-z) where a-z are macros registered, Select the macro file in lxterm.cfg\n" 
"        : m + '2' will display all the registered macros\n"
"        : m + '1' will refresh the macros specified in macro file\n"
"'#'     : Start firmware update according to proprietary protocol, binary file should be fw.bin\n"
"'d'     : Toggle between hex display and ascii display mode\n"
"'U'     : Upload wisun configuration, requires wsnconfig executable\n"
"'*'     : Upload a file using proprietary protocol\n"
"'x'     : Exit this app\n";


