TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
TARGET = mukom

OBJECTS_DIR = qtb/obj
DESTDIR = qtb


SOURCES += \
    key-server.cpp \
    lxt.cpp \
    option.c

LIBS += -pthread
