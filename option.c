
#include <stdarg.h>
#include "option.h"


typedef struct olist_ {
	char *key;
	char *value;
	struct olist_ *next;
}olist;

struct options_ {
	FILE *file;
	char *filename;
	olist *list;
} options;


static int getnextline(char *line, int maxlen)
{
	int n = 0;
	while (1) {
		char ch = fgetc(options.file);
		if (feof(options.file)) break;
		if ((ch == '\r') || (ch == '\n')) {
			line[n] = ch;
			line[n+1] = 0;
			return n;
		}
		else {
			if (n < (maxlen - 1)) {
				line[n++] = ch;
			}
		}
	}
	return -1;
}


int setoptionsfile(char *filename) {
	options.filename = filename;
	options.file = fopen(options.filename, "rt");
	if (!options.file) {
		printf("%s cannot be opened\n", options.filename);
		return -1;
	}
	printf("reading options from %s\n", options.filename);
	return 0;
}


int readoptionsfile(char *filename) {
/*
	char *key, *value;
	char str[128];

	options.filename = filename;
	file = fopen(options.filename, "rt");
	if (!file) {
		printf("%s cannot be opened\n", options.filename);
		return -1;
	}
	printf("reading options from %s\n", options.filename);

	options.list = NULL;

	while (1) {
		int slen;
		olist *list;

		int n = getline(line, 256);
		if (n <= 0) {
			return -1;
		}
		int n = sscanf(line, "%s", str);
		if (n != 1) {
			printf("error scaning [%s]\n", line);
			continue;
		}
		slen = strlen(str);
		if (str[0] == '#') continue;
		key = new char[slen + 1];
		if (key) {
			strncpy(key, str, slen);
		}
		n = sscanf(line, "%s", str);
		if (n != 1) {
			printf("error scaning [%s]\n", line);
			continue;
		}
		slen = strlen(str);
		value = new char[slen + 1];
		if (value) {
			strncpy(value, str, slen);
		}
		if (!key || !value) {
			printf("error in options file or memory error\n");
			fclose(options.file);
			return -1;
		}

		list = new olist[1];
		if (list) {
			list->key = key;
			list->value = value;
			list->next = NULL;

			if (options.list) {
				list->next = options.list;
			}
			options.list = list;
		}
	}
*/
	return 0;
}


int getoption(const char *key, const char *format,  ...)
{
	/*
	value = NULL;
	for (olist *list = options.list; list != NULL; list = list->next) {
		if (strcmp(list->key, key) == 0) {
			value = list->value;
			return 1;
		}
	}
	return 0;
	*/

	char line[256];
	char str[256];
	va_list args;

	if (!options.file) return 0;

	rewind(options.file);

	while (1) {
		int slen;
		int n = getnextline(line, 256);
		if (n < 0) {
			return -1;
		}
		if (n == 0) continue;
		n = sscanf(line, "%s", str);
		if (n != 1) {
			printf("error scaning [%s]\n", line);
			continue;
		}

		if (str[0] == '#') continue;
		if (strcmp(str, key)) continue;
		slen = strlen(str);
		va_start(args,format);
		n = vsscanf(&line[slen],format, args);
		va_end(args);

		printf("option: %s", line);
		return n;

	}
	printf("option %s not found, using default\n", key);
	return 0;
}
