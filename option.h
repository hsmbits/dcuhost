
#ifndef options_header
#define options_header

#include <stdio.h>
#include <string.h>

// Fixing checkin issues

#ifdef __cplusplus
extern "C" {
#endif

int getoption(const char *key, const char *format, ...);
int setoptionsfile(char *filename);

#ifdef __cplusplus
}
#endif

#endif
